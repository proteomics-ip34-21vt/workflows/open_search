# Open search workflow
This workflow handles a complete a complete open search proteomics analysis. The input are LC-MS files.
The main stepas are: 
1.  Create a workspace
2.  Download a database
3.  Search with MSFragger
4.  PeptideProphet
5.  ProteinProphet
6.  Filter
7.  Report

More details can be found [here](https://github.com/Nesvilab/philosopher/wiki/Open-Search-Analysis).

## Pipeline
The pipeline runs a small tmt workflow and tests for certain output files. Take a look at
`.gitlab-ci.yml` to learn how that exactly works.
