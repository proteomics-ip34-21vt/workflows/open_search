#!/usr/bin/env nextflow

nextflow.enable.dsl=2

params.input_dir = ''
params.output_dir = ''
params.ram = ''
params.db_id = '' // human: UP000005640
params.custom_fasta = ''

include { INIT_WORKSPACE } from './modules/init_workspace'
include { MSFRAGGER } from './modules/msfragger'
include { BUILD_FASTA } from './modules/build_fasta'
include { PEPTIDE_PROPHET } from './modules/peptide_prophet'
include { PROTEIN_PROPHET } from './modules/protein_prophet'
include { FILTER_FDR } from './modules/filter_fdr'
include { REPORT } from './modules/report'
include { FLAG_CUSTOMS } from './modules/flag_customs.nf'

workflow {
	INIT_WORKSPACE()

	custom_fasta = params.custom_fasta
	if (!custom_fasta) {
		file = File.createTempFile("nf_temp", ".fasta")
		custom_fasta = file.absolutePath
	}

	BUILD_FASTA(INIT_WORKSPACE.out.meta, params.db_id, custom_fasta)

	MSFRAGGER(BUILD_FASTA.out.db, params.input_dir, params.ram, "open", "")
	PEPTIDE_PROPHET(BUILD_FASTA.out.meta, BUILD_FASTA.out.db, MSFRAGGER.out.pepXML, "--nonparam --expectscore --decoyprobs --masswidth 1000.0 --clevel -2")
	PROTEIN_PROPHET(PEPTIDE_PROPHET.out.meta, BUILD_FASTA.out.db, PEPTIDE_PROPHET.out.pep_xml, "--maxppmdiff 2000000")
	
	group_dirs = Channel.fromPath(params.input_dir + "/*", type:"dir" ) // from here workflows will run parallel with one item of 'group_dirs'
	FILTER_FDR(PROTEIN_PROPHET.out.meta, PEPTIDE_PROPHET.out.pep_xml, PROTEIN_PROPHET.out.interact_prot, "", group_dirs)
	REPORT(FILTER_FDR.out, PEPTIDE_PROPHET.out.pep_xml, PROTEIN_PROPHET.out.interact_prot)

	custom_fasta = params.custom_fasta
	if (custom_fasta) {
		FLAG_CUSTOMS(custom_fasta, REPORT.out.report_dir)
	}
}
